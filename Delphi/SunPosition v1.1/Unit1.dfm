�
 TFORM1 03  TPF0TForm1Form1Left Top BorderIconsbiSystemMenu BorderStylebsSingleCaptionSun Position CalculatorClientHeight�ClientWidthGColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style Menu	MainMenu1OldCreateOrderPositionpoMainFormCenterVisible	OnCreate
FormCreate	OnDestroyFormDestroy
OnKeyPressFormKeyPressOnMouseDownFormMouseDownOnShowFormShowPixelsPerInch`
TextHeight TLabellblLongitudeLeftTop.Width/HeightCaption	LongitudeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFont  TLabellblLatitudeLeftTopWidth'HeightCaptionLatitudeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFont  TLabel
lblLatNoteLeft� TopWidth� HeightCaption+ve for North, -ve for South  TLabellblLongNoteLeft� Top.Width� HeightCaption+ve for East, -ve for West  TLabellblTimeZoneLeftTopRWidth1HeightCaption	Time ZoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFont  TLabellblPlaceNameLeftToptWidth7HeightCaption
Place NameFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFont  TLabellblAbrevLeft�ToptWidth7HeightCaptionAbreviation  TLabellblMsgLeft� TopQWidthHeightBiDiModebdLeftToRightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameTahoma
Font.Style ParentBiDiMode
ParentFont  TButtonbtnCalcLeft	Top� WidthbHeightCaption	CalculateTabOrderOnClickbtnCalcClick  TEditedLongitudeLeftSTop*WidthTHeightAutoSize	MaxLengthTabOrderOnEnteredEnterOnExitedLongitudeExit
OnKeyPressNumberKeyPress  TRadioGrouprgFormatLeftYTopWidth� Height=CaptionLatitude/Longitude Entry FormatTabOrder  TRadioButtonrbDegMinSecLeftgTopWidth� HeightCaptionDegrees.Minutes.SecondsTabOrder	OnClickFormatChange  TRadioButtonrbDecDegLeftgTop*WidthqHeightCaptionDecimal Degrees Checked	TabOrder
TabStop	OnClickFormatChange  TEdit
edLatitudeLeftSTopWidthTHeightAutoSize	MaxLength
TabOrder OnEnteredEnterOnExitedLatitudeExit
OnKeyPressNumberKeyPress  	TComboBoxedZoneLeftSTopNWidthTHeightAutoCompleteStylecsOwnerDrawVariableTabOrderOnEnteredEnterOnExit
edZoneExitItems.Strings	UTC+14:00	UTC+13:00	UTC+12.45	UTC+12:00	UTC+11:00	UTC+10.30	UTC+10:00UTC+9:30UTC+9:00UTC+8:45UTC+8:30UTC+8:00UTC+7:00UTC+6:30UTC+6:00UTC+5:45UTC+5:30UTC+5:00UTC+4:30UTC+4:00UTC+3:30UTC+3:00UTC+2:00UTC+1:00UTC+0:00UTC-1:00UTC-2:00UTC-3:00UTC-3:30UTC-4:00UTC-4:30UTC-5:00UTC-6:00UTC-7:00UTC-8:00UTC-9:00UTC-9:30	UTC-10:00	UTC-11:00	UTC-12:00   TEditedPlaceLeftSToppWidthHeight	MaxLengthPTabOrderOnEnteredEnterOnExitedPlaceExit  TEditedAbrevLeft�ToppWidthsHeightHintAbreviation of Place Name	MaxLengthTabOrderOnEnteredEnter  TButtonbtnExpLeftqTop� Width^HeightCaptionExport to MacroTabOrderOnClickbtnExpClick  TButtonbtnExitLeft�Top� Width^HeightCaptionExitTabOrderOnClickbtnExitClick  	TMainMenu	MainMenu1LeftTopL 	TMenuItemHelp1CaptionHelp 	TMenuItemShadowInstructionManual1CaptionShadow Instruction ManualOnClickShadowInstructionManual1Click  	TMenuItemAbout1CaptionAboutOnClickAbout1Click     