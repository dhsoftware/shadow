object fCAD: TfCAD
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Specify CAD Program'
  ClientHeight = 145
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 285
    Height = 13
    Caption = 'Select the CAD program that you are using this macro with:'
  end
  object rb18: TRadioButton
    Left = 32
    Top = 40
    Width = 249
    Height = 17
    Caption = 'DataCAD 18 or earlier'
    TabOrder = 0
    OnClick = rbClick
  end
  object rb19: TRadioButton
    Left = 32
    Top = 63
    Width = 249
    Height = 17
    Caption = 'DataCAD 19 or later'
    TabOrder = 1
    OnClick = rbClick
  end
  object rbSpirit: TRadioButton
    Left = 32
    Top = 86
    Width = 249
    Height = 17
    Caption = 'Spirit'
    TabOrder = 2
    OnClick = rbClick
  end
  object btnOK: TButton
    Left = 218
    Top = 101
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = btnOKClick
  end
end
