unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ShellApi, System.iniFiles,
  System.UITypes;

CONST
  Version = 'v1.3.2.0';

type
  TShadow = class(TForm)
    Shape1: TShape;
    Panel1: TPanel;
    lblwww: TLabel;
    lblCopyright: TLabel;
    lblShadow: TLabel;
    lblVersion: TLabel;
    lblContribute: TLabel;
    btnInstructions: TButton;
    mCopyright: TMemo;
    btnDismiss: TButton;
    lblErr: TLabel;
    btnCAD: TButton;
    btnAgree: TButton;
    lblYear: TLabel;
    procedure btnDismissClick(Sender: TObject);
    procedure btnInstructionsClick(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
    procedure lblwwwClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCADClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAgreeClick(Sender: TObject);
  private
    { Private declarations }
    CadVer : double;
  public
    { Public declarations }
  end;

var
  Shadow: TShadow;

implementation

{$R *.dfm}

uses ProgramSelection, MsgStrings;

procedure TShadow.btnAgreeClick(Sender: TObject);
begin
  btnAgree.Visible := false;
  fCAD.ShowModal;
  Application.Terminate;
end;

procedure TShadow.btnCADClick(Sender: TObject);
begin
  fCAD.ShowModal;
end;

procedure TShadow.btnDismissClick(Sender: TObject);
begin
  Shadow.Close;
end;

procedure TShadow.btnInstructionsClick(Sender: TObject);
var
  helpfilename : PWideChar;
begin
  try
    helpfilename := PWideChar(ExtractFilePath(ParamStr(0)) + 'ShadowInstructions.pdf');
    ShellExecute(0, 'open', helpfilename,nil,nil, SW_SHOWNORMAL) ;
    Shadow.Close;
   except
    on E: Exception do
      lblErr.Caption := E.ClassName + ': ' + E.Message;
  end;
end;

procedure TShadow.FormCreate(Sender: TObject);
var
  str : string;
  SettingsFile : TIniFile;
  colon : integer;
begin
  if ParamCount > 0 then begin
    str := paramstr(1);
    colon := str.IndexOf(':');
    if colon<0 then
      lblVersion.Caption := str
    else begin
      lblVersion.Caption := str.Substring(0, colon);
      lblYear.Caption := str.Substring(colon+1);
    end;
  end
  else
    lblVersion.Caption := '';

  SettingsFile := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\Shadow.ini');
  CadVer := SettingsFile.ReadFloat('General', 'DCAD_Version', 0.0);
  if CadVer = 0.0 then begin
    btnAgree.visible := true;
    btnDismiss.Caption := 'Exit Macro';
  end;

  MsgStrings.Msgs := TStringList.Create;
  str := ExtractFilePath(Application.ExeName) + 'Shadow.msg';
  Msgs.LoadFromFile(str);
end;


procedure TShadow.FormDestroy(Sender: TObject);
begin
  Msgs.Free;
end;

procedure TShadow.FormShow(Sender: TObject);
begin
  btnCAD.Caption := msgs[115];  // Select CAD Program
  btnInstructions.Caption := msgs[63];   // Show Instruction Manual
  lblContribute.Caption := msgs[127];
  if CadVer > 0 then
    btnDismiss.Caption := msgs[117];    // Dismiss
end;

procedure TShadow.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TShadow.lblwwwClick(Sender: TObject);
begin
ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

end.
