program ShadHelp;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Shadow},
  ProgramSelection in 'ProgramSelection.pas' {fCAD},
  MsgStrings in 'MsgStrings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TShadow, Shadow);
  Application.CreateForm(TfCAD, fCAD);
  Application.Run;
end.
