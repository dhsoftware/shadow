unit ProgramSelection;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.iniFiles, System.UITypes;

type
  TfCAD = class(TForm)
    Label1: TLabel;
    rb18: TRadioButton;
    rb19: TRadioButton;
    rbSpirit: TRadioButton;
    btnOK: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure rbClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ver_prev : double;
  public
    { Public declarations }
  end;

var
  fCAD: TfCAD;

implementation

{$R *.dfm}

uses MsgStrings;

procedure TfCAD.btnOKClick(Sender: TObject);
var
  SettingsFile : TIniFile;
  ver : double;
  showmsg : boolean;
begin
  ver := 0.0;
  showMsg := false;
  if rbSpirit.Checked then begin
    ver := 1.0;
    showmsg := (ver_prev <> 1.0);
  end;
  if rb18.Checked then begin
    ver := 18.0;
    showmsg := (ver_prev <> 18.0);
  end;
  if rb19.Checked then begin
    ver := 190100.0;
    showmsg := (ver_prev <= 18.0);
  end;

  SettingsFile := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\Shadow.ini');
  SettingsFile.WriteFloat('General', 'DCAD_Version', ver);
  SettingsFile.Free;

  if showmsg and (ver_prev > 0.0) then
    messagedlg (msgs[122], mtInformation, [mbOK], 0);   // Exit and re-start the macro for the CAD program change to be effective

  ModalResult := 1;
end;

procedure TfCAD.FormCreate(Sender: TObject);
var
  SettingsFile : TIniFile;
begin
  SettingsFile := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\Shadow.ini');
  ver_prev := SettingsFile.ReadFloat ('General', 'DCAD_Version', 0.0);
  SettingsFile.Free;
  if ver_prev = 1.0 then
    rbSpirit.Checked := true
  else if ver_prev = 18.0 then
    rb18.Checked := true
  else if ver_prev > 18.0 then
    rb19.Checked := true
  else begin
    btnOK.Enabled := false;
    if ver_prev <> 0.0 then
      ShowModal;
  end;
end;

procedure TfCAD.FormShow(Sender: TObject);
begin
  Caption := msgs[118];   //  Select CAD Program
  Label1.Caption := msgs[119];  // Select the CAD program that you are using this macro with:
  rb18.Caption := 'DataCAD 18 ' + msgs[120];   // or earlier
  rb19.Caption := 'DataCAD 19 ' + msgs[121];   // or later
end;

procedure TfCAD.rbClick(Sender: TObject);
begin
  btnOK.Enabled := true;
end;

end.
