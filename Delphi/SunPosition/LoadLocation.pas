unit LoadLocation;

interface

uses
  CommonStuff, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  System.IniFiles, System.UITypes, ShlObj, ComObj, ActiveX;

type
  TfmLocations = class(TForm)
    cbCountry: TComboBox;
    cbCity: TComboBox;
    lblCountry: TLabel;
    lblCity: TLabel;
    lblLatitude: TLabel;
    lblLongitude: TLabel;
    lblTimeZone: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cbCountryChange(Sender: TObject);
    procedure cbCityChange(Sender: TObject);
  private
    { Private declarations }
    datafilename : string;
    ini : TMeminifile;
    iniSections : TStringList;
  public
    { Public declarations }
    dataavailable : boolean;
    longitude, latitude, timezone : double;
    szone : string;
    sCountry, sCity : string;
  end;

var
  fmLocations: TfmLocations;

implementation

{$R *.dfm}

procedure TfmLocations.cbCityChange(Sender: TObject);
var
  direction : string;
  x : double;
  d, h, m, s : integer;
begin
  if (cbCity.Text <= '') or not dataavailable then begin
    cbCountry.Items.Clear;
    lblLatitude.Caption := '';
    lblLongitude.Caption := '';
    lblTimeZone.Caption := '';
    exit;
  end;
  sCountry := cbCountry.Text;
  sCity := cbCity.Text;
  ini := TMeminifile.Create(datafilename);
  iniSections := TStringList.Create;
  try
    // longitude
    longitude := ini.ReadFloat(cbCity.Text, 'longitude', 0);
    if longitude < 0 then begin
      direction := 'W';
    end
    else
      direction := 'E';
    d := abs(Trunc(longitude));
    x := (abs(longitude) - d)*60;
    m := Trunc(x);
    x := (x - m)*60;
    s := Round(x);
    lblLongitude.Caption := MyMsgs[15] + ': ' + d.ToString + ' �  ' + m.ToString + ' ''  ' + s.ToString + ' "  ' + direction;

    //latitude
    latitude := ini.ReadFloat (cbCity.Text, 'latitude', 0);
    if latitude < 0 then begin
      direction := 'S';
    end
    else
      direction := 'N';
    d := abs(Trunc(latitude));
    x := (abs(latitude) - d)*60;
    m := Trunc(x);
    x := (x - m)*60;
    s := Round(x);
    lblLatitude.Caption := MyMsgs[14] + ': ' + d.ToString + ' �  ' + m.ToString + ' ''  ' + s.ToString + ' "  ' + direction;

    // time zone
    timezone := ini.ReadFloat (cbCity.Text, 'timezone', 0);
    h := Trunc(timezone);
    m := round ((timezone - h) * 60);
    sZone := 'UTC';
    if h >=0 then
      sZone := sZone + '+';
   sZone := sZone + h.ToString + ':';
    if m < 10 then
      sZone := sZone + '0';
    sZone := sZone + m.ToString;
    lblTimeZone.Caption := MyMsgs[16] + ': ' + sZone;
  finally
    ini.Free;
    iniSections.Free;
  end;
end;

procedure TfmLocations.cbCountryChange(Sender: TObject);
var
  ThisTown : string;
begin
  cbCity.Items.Clear;
  if not dataavailable then begin
    cbCountry.Items.Clear;
    exit;
  end;
  ini := TMeminifile.Create(datafilename);
  iniSections := TStringList.Create;
  try
    ini.ReadSections(iniSections);
    for ThisTown in iniSections do begin
      if ini.ReadString(ThisTown, 'country', '') = cbCountry.Text then
        cbCity.Items.Add(ThisTown);
    end;
  finally
    ini.Free;
    iniSections.Free;
  end;
  cbCity.ItemIndex := 0;
  cbCityChange(Sender);
end;

procedure TfmLocations.FormCreate(Sender: TObject);
type
 TShellLinkInfo = record
  PathName: string;
  Arguments: string;
  Description: string;
  WorkingDirectory: string;
  IconLocation: string;
  IconIndex: integer;
  ShowCmd: integer;
  HotKey: word;
 end;
var
  i : integer;
  ThisTown, ThisCountry : string;
  //ShellLink: IShellLink;
  LinkInfo: TShellLinkInfo;

  procedure GetShellLinkInfo(const LinkFile: String;
                             var SLI: TShellLinkInfo);
  { Retrieves information on an existing shell link }
  var
   SL: IShellLink;
   PF: IPersistFile;
   FindData: TWin32FindData;
   AStr: array[0..MAX_PATH] of char;
  begin
   OleCheck(CoCreateInstance(CLSID_ShellLink, nil, CLSCTX_INPROC_SERVER,
    IShellLink, SL));
   { The IShellLink implementer must also support the IPersistFile }
   { interface. Get an interface pointer to it. }
   PF := SL as IPersistFile;
   { Load file into IPersistFile object }
   OleCheck(PF.Load(PWideChar(LinkFile), STGM_READ));
   { Resolve the link by calling the Resolve interface function. }
   OleCheck(SL.Resolve(0, SLR_ANY_MATCH or SLR_NO_UI));
   { Get all the info! }
   with SLI do
   begin
    OleCheck(SL.GetPath(AStr, MAX_PATH, FindData, SLGP_SHORTPATH));
    PathName := AStr;
    OleCheck(SL.GetArguments(AStr, MAX_PATH));
    Arguments := AStr;
    OleCheck(SL.GetDescription(AStr, MAX_PATH));
    Description := AStr;
    OleCheck(SL.GetWorkingDirectory(AStr, MAX_PATH));
    WorkingDirectory := AStr;
    OleCheck(SL.GetIconLocation(AStr, MAX_PATH, IconIndex));
    IconLocation := AStr;
    OleCheck(SL.GetShowCmd(ShowCmd));
    OleCheck(SL.GetHotKey(HotKey));
   end;
  end;


begin
  Caption := MyMsgs[27];
  lblCountry.Caption := MyMsgs[24];
  lblCity.Caption := MyMsgs[25];
  btnOK.Caption := MyMsgs[26];
  btnCancel.Caption := MyMsgs[18];
  cbCountry.Items.Clear;
  cbCity.Items.Clear;
  datafilename := ExtractFilePath(Application.ExeName);
  dataavailable := fileexists (datafilename + 'geogdata.ini');
  if dataavailable then
    datafilename := datafilename + 'geogdata.ini'
  else begin
    i := pos ('SUPPORT FILES', UpperCase(datafilename));
    dataavailable :=  i > 0;
    if dataavailable then begin
      delete (datafilename, i, length(datafilename)-i+1);
      datafilename := datafilename + 'Sun Shader\NX\Support Files\geogdata.ini';
      dataavailable := fileexists (datafilename);
    end;
  end;
  if not dataavailable then begin
    datafilename := ExtractFilePath(Application.ExeName) + 'geogdata.ini.lnk';
    dataavailable := fileexists (datafilename);
    if not dataavailable then begin
      datafilename := ExtractFilePath(Application.ExeName) + 'geogdata.ini - Shortcut.lnk';
      dataavailable := fileexists (datafilename);
    end;
    if dataavailable then begin
      try
        GetShellLinkInfo (datafilename, LinkInfo);
        datafilename := LinkInfo.PathName;
        dataavailable := fileexists(datafilename);
      except
        dataavailable := false;
      end;
    end
  end;
  if dataavailable then begin
    cbCountry.Items.Add ('Select Country');
    ini := TMeminifile.Create(datafilename);
    iniSections := TStringList.Create;
    try
      ini.ReadSections(iniSections);
      for ThisTown in iniSections do begin
        ThisCountry := ini.ReadString(ThisTown, 'country', '');
        if cbCountry.Items.IndexOf(ThisCountry) < 0 then
          cbCountry.Items.Add(ThisCountry);
      end;
    finally
      ini.Free;
      iniSections.Free;
    end;
    cbCountry.ItemIndex := 0;
  end
  else begin
    MessageDlg(MyMsgs[23] {Sunshader Data File not found}, mtError, [mbOK], 0);
  end;

end;

end.
