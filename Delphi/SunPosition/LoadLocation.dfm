object fmLocations: TfmLocations
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Sunshader Locations'
  ClientHeight = 282
  ClientWidth = 227
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblCountry: TLabel
    Left = 11
    Top = 13
    Width = 39
    Height = 13
    Caption = 'Country'
  end
  object lblCity: TLabel
    Left = 11
    Top = 46
    Width = 19
    Height = 13
    Caption = 'City'
  end
  object lblLatitude: TLabel
    Left = 56
    Top = 179
    Width = 49
    Height = 13
    Caption = 'lblLatitude'
  end
  object lblLongitude: TLabel
    Left = 56
    Top = 198
    Width = 57
    Height = 13
    Caption = 'lblLongitude'
  end
  object lblTimeZone: TLabel
    Left = 56
    Top = 217
    Width = 56
    Height = 13
    Caption = 'lblTimeZone'
  end
  object cbCountry: TComboBox
    Left = 56
    Top = 10
    Width = 163
    Height = 21
    Style = csDropDownList
    TabOrder = 0
    OnChange = cbCountryChange
  end
  object cbCity: TComboBox
    Left = 56
    Top = 43
    Width = 163
    Height = 21
    Style = csDropDownList
    TabOrder = 1
    OnChange = cbCityChange
  end
  object btnOK: TButton
    Left = 88
    Top = 249
    Width = 131
    Height = 25
    Caption = 'Use this Location'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 8
    Top = 249
    Width = 57
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
