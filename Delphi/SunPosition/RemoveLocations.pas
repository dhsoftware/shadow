unit RemoveLocations;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.IniFiles, CommonStuff,
  System.UITypes;

type
  TfmRemoveLocations = class(TForm)
    lbLocations: TListBox;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
	  Ini	: TIniFIle;
    iniSections : TStringList;
    Abrevs,
    Names : TStringList;

  public
    { Public declarations }
  end;

var
  fmRemoveLocations: TfmRemoveLocations;

implementation

{$R *.dfm}

procedure TfmRemoveLocations.btnOKClick(Sender: TObject);
var
  i : integer;
  SelectedAbrev, SelectedNames : TStringList;
  Prompt, str : string;
begin
  SelectedAbrev := TStringList.Create;
  SelectedNames := TStringList.Create;
  Prompt := '';
  try
    for i := 0 to lbLocations.Items.Count-1 do begin
      if lbLocations.Selected[i] then begin
        SelectedAbrev.Add (Abrevs[i]);
        SelectedNames.Add (Names[i]);
        Prompt := Prompt + Abrevs[i] + ', ';
      end;
    end;
    if SelectedNames.count < 1 then begin
      MessageDlg (MyMsgs[21], mtInformation, [mbOK], 0);
      modalresult := mrCancel;
    end
    else begin
      if MessageDlg (MyMsgs[22] + sLineBreak + Prompt, mtConfirmation, [mbOK, mbCancel], 0) = mrOK then begin
        for str in SelectedNames do begin
          Ini.EraseSection('Location: ' + str);
        end;
        modalresult := mrOK;
      end;

    end;
  finally
    SelectedAbrev.Free;
    SelectedNames.Free;
  end;
end;

procedure TfmRemoveLocations.FormCreate(Sender: TObject);
var
  i : integer;
  str, str1 : string;
begin
  Caption := MyMsgs[19];
  btnCancel.Caption := MyMsgs[18];
  btnOK.Caption := MyMsgs[20];

 	Ini := TIniFile.Create (ExtractFilePath(Application.ExeName) + 'shadow.ini');
  iniSections := TStringList.Create;
  Abrevs := TStringList.Create;
  Names := TStringList.Create;
  ini.ReadSections(iniSections);
  if iniSections.Count > 0 then begin
    for i := iniSections.count-1 downto 0 do begin
      str := iniSections[i];
      if str.StartsWith('Location: ') then begin
        str1 := ini.ReadString(str, 'Place Abrev', '');
        Abrevs.Add(str1);
        Names.Add(copy (str, 11, length(str)-10));
        if trim(str1) > '' then
          str1 := str1 + ' - ';
        str1 := str1 + copy (str, 11, length(str)-10);
        lbLocations.Items.Add(str1);
      end;
    end;
  end;
end;

procedure TfmRemoveLocations.FormDestroy(Sender: TObject);
begin
  Ini.Free;
  iniSections.Free;
  Abrevs.Free;
  Names.Free;
end;

end.
