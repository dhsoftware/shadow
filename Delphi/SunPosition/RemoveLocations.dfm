object fmRemoveLocations: TfmRemoveLocations
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Remove Previous Locations'
  ClientHeight = 236
  ClientWidth = 223
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    223
    236)
  PixelsPerInch = 96
  TextHeight = 13
  object lbLocations: TListBox
    Left = 8
    Top = 8
    Width = 207
    Height = 191
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 0
    ExplicitWidth = 217
    ExplicitHeight = 201
  end
  object btnOK: TButton
    Left = 72
    Top = 206
    Width = 143
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Remove Selected Locations'
    Default = True
    TabOrder = 1
    OnClick = btnOKClick
    ExplicitTop = 216
    ExplicitWidth = 153
  end
  object btnCancel: TButton
    Left = 8
    Top = 206
    Width = 58
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    ExplicitTop = 216
  end
end
