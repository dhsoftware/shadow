program SunPos;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  About in 'About.pas' {fmAbout},
  MakeAbbreviation in 'MakeAbbreviation.pas',
  LoadLocation in 'LoadLocation.pas' {fmLocations},
  CommonStuff in 'CommonStuff.pas',
  RemoveLocations in 'RemoveLocations.pas' {fmRemoveLocations};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfmAbout, fmAbout);
//  Application.CreateForm(TfmLocations, fmLocations);
//  Application.CreateForm(TfmRemoveLocations, fmRemoveLocations);
  Application.Run;
end.
