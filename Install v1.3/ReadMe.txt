PLEASE READ THE LICENCE AGREEMENT AT THE END OF THIS README FILE.
IF YOU AGREE TO THIS AGREEMENT WITH THE AUTHOR THEN USE A PASSWORD OF 'AGREE' TO UNZIP
THE MACRO FILES (note that the password is case sensitive and must be entered in
all upper case).

This version of the macro will cast shadows of most entity types except for DataCAD's 
"Smart" entities and Spirit's "Architecture" entities. 
Note that polygons and slabs with more than 36 sides (in DataCAD versions 14 onwards) 
will not be processed fully (refer to instruction document for details).
If you would like to be advised of future versions, please visit my web site at 
dhsoftware.com.au and join my mailing list.

TO INSTALL FOR DATACAD:
This zip file contains a folder structure that reflects the structure of a typical DataCAD 
installation.
If you have a typical installation (where the Macros and Support Files folders
are located directly within your main DataCAD folder) then simply extract the 'Macros' and
'Support Files' folders from the zip file into your main DataCAD folder to maintain the
directory structure from the zip file (note that your main DataCAD folder normally includes 
the version number - e.g. for version 22 the folder would normally be named 'DataCAD 22').

If you do not have a typical DataCAD installation, then ensure to extract the SHADOW.DCX
file to your 'Macros' folder, and to extract the 'dhSoftware' folder from the zip file into
your Support Files folder.

TO INSTALL FOR SPIRIT:
Note that although the macro may work with Spirit, it was not found to be reliable in testing
with the most recent version (macro exited without explanation at times, although it did run
correct results were obtained).  The use of the macro with Spirit is NOT RECOMMENDED.
If you do wish to install for use with Sprit then use the following instructions: 
Do not copy the directory structure from the zip file. 
Extract all files (from both the 'Macros' and 'Support Files/dhsoftware' 
folders) into Spirit's '130_Macros' folder (e.g. for Spirit 2021 this would typiclly be 
at 'C:\ProgramData\STI\SPIRIT2021\130_Macros').


BY INSTALLING AND USING THIS MACRO YOU AGREE TO THE BELOW LICENCE AGREEMENT.

 This software is distributed free of charge and on an "AS IS" basis. To the extent 
 permitted by law, I disclaim all warranties of any kind, either express or implied,
 including but not limited to, warranties of merchantability, fitness for a particular purpose,
 and non-infringement of third-party rights.
 
 I will not be liable for any direct, indirect, actual, exemplary or any other damages
 arising from the use or inability to use this software, even if I have been advised of the
 possibility of such damages.
 
 Whilst I do solicit contributions toward the cost of developing and distributing my
 software, such contributions are entirely at your discretion and in no way constitute a
 payment for the software.
 
 You may distribute this software to others provided that you distribute the complete
 unaltered zip file provided by me at the dhsoftware.com.au web site, and that you do
 so free of charge. This includes not charging for any other software, service or 
 product that you associate with this software in such a way as to imply that a purchase
 is required in order to obtain this software (without limitation, examples of unacceptable
 charges would be charging for distribution media or for any accompanying software that is
 on the same media or contained in the same download or distribution file).  If you wish
 to make any charge at all you need to obtain specific permission from me.
 
 Whilst it is free (or because of this), I would like and expect that if you can think
 of any improvements or spot any bugs (or even spelling or formatting errors in the
 documentation) that you would let me know. Your feedback may help with future
 development of the macro.


