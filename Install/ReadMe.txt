Please refer to the ShadowInstructions.pdf document that has been copied into your macro folder.
You can access this document by selecting S9 from the macro main menu.
You will require Adobe Reader (or similar) software to read it.

This version of the macro is limited to casting shadows of Polygons and Lines. Note that polygons with more than 36 sides in DataCAD versions 14 onwards will not be processed fully (refer to instruction document for details).
If you would like to be advised of future versions, please visit my web site at dhsoftware.com.au and join my mailing list.

Bug reports or enhancement requests can be sent to dhsoftware1@gmail.com

Thank you,
David Henderson
dhsoftware1@gmail.com
www.dhsoftware.com.au